package com.example.counterapp

import android.app.Service
import android.content.Intent
import android.os.Binder
import android.os.IBinder
import android.util.Log
import android.widget.Toast


class CounterService: Service() {
    private val TAG = "CounterService"
    private val binder = CounterBinder()
    private var counter = 0

    fun increment() {
        counter++
        Log.d(TAG, counter.toString())
    }

    fun decrement() {
        counter--
        Log.d(TAG, counter.toString())
    }

    fun showCurrentCount() {
        val context = applicationContext
        Toast.makeText(context, counter.toString(), Toast.LENGTH_SHORT).show()
        Log.d(TAG, counter.toString())
    }

    inner class CounterBinder : Binder() {
        fun getService(): CounterService = this@CounterService
    }

    override fun onBind(intent: Intent?): IBinder {
        return binder
    }
}